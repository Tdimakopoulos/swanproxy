/*
 * Copyright (c) 2014 EXODUS S.A.
 * SwaniCare
 * Author : Dr. Thomas Dimakopoulos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.eu.exus.swan.network;

import org.eu.exus.swan.db.values.StoreValues;
import java.time.Instant;

import java.io.*;
import org.eu.exus.swan.calc.Calculations;
import org.eu.exus.swan.calc.CalculationsPH;
import org.exus.rnd.file.FileUtils;
import org.exus.swan.phcal.CalibrationManager;
import org.swan.types.osb.osbtypes;

public class SnpdPacket {

    int idebug = 0;
    private Instant timestamps;
    private DataType dataTypes;

    private Instant timestampstr;

    private int MMP96302B;
    private int MMP98502B;
    private int MMPSENSSTATUS1B;

    private int pH11B;
    private int pH21B;
    private int pH1SNESSTATUS1B;
    private int pH2SENS_STATUS1B;
    private int T11B;
    private int T21B;
    private int T31B;
    private int T1SENSSTATUS1B;
    private int T2SENSSTATUS1B;
    private int T3SENSSTATUS1B;
    private int CBP1B;
    private int CBP_STATUS1B;
    private int BATTSOC1B;
    private int BATTCAP2B;
    private int BATTV2B;
    private int BATTI2B;
    private int BATTTEMP2B;
    private int BATTSTATUS1B;
    private int IWSDATTITUDE1B;
    private int IWSDSTATUS1B;

    private int data;

    Double ph11 = new Double(100);
    Double ph12 = new Double(1000);
    Double ph13 = new Double(2000);

    Double ph21 = new Double(100);
    Double ph22 = new Double(1000);
    Double ph23 = new Double(2000);

    public enum DataType {
        SENSOR_MMP,
        SENSOR_TNFA,
        SENSOR_CRP,
        SENSOR_TEMP,
        SENSOR_PH,
        SENSOR_BACTERIA,
        FUSION,
        WARNING,
        TEST,
        TREATMENT,//9
        WOUND_ALARM,//10
        ACCEL_DATA_X,//11
        ACCEL_DATA_Y,//12
        ACCEL_DATA_Z,//13
        IWSDDATA,//14
        Low_Pressure_Alarm,//15
        High_Pressure_Alarm,//16
        Over_Pressure_Alarm,//17
        Time_Stamp//19
    }

    public enum FusionStatus {
        FUSION_FAIL,
        WOUND_OK,
        WOUND_NOT_OK
    }

    FileUtils pps;

    public SnpdPacket(byte[] inputData, Instant connectionTimestamp, Long patid, int T1, int T2, int T3, FileUtils ppa) {
        int[] sizes;
        int[] heads;
        sizes = new int[]{8, 2, 8, 6, 6, 2, 6, 6, 2, 2, 6, 6, 6, 2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 2, 2, 2, 5};
        heads = new int[]{0, 8, 8 + 2, 8 + 2 + 8, 8 + 2 + 8 + 6, 8 + 2 + 8 + 6 + 6, 8 + 2 + 8 + 6 + 6 + 2, 8 + 2 + 8 + 6 + 6 + 2 + 6, 8 + 2 + 8 + 6 + 6 + 2 + 6 + 6, 8 + 2 + 8 + 6 + 6 + 2 + 6 + 6 + 2, 8 + 2 + 8 + 6 + 6 + 2 + 6 + 6 + 2 + 2,
            8 + 2 + 8 + 6 + 6 + 2 + 6 + 6 + 2 + 2 + 6, 8 + 2 + 8 + 6 + 6 + 2 + 6 + 6 + 2 + 2 + 6 + 6,
            8 + 2 + 8 + 6 + 6 + 2 + 6 + 6 + 2 + 2 + 6 + 6 + 6, 8 + 2 + 8 + 6 + 6 + 2 + 6 + 6 + 2 + 2 + 6 + 6 + 6 + 2,
            8 + 2 + 8 + 6 + 6 + 2 + 6 + 6 + 2 + 2 + 6 + 6 + 6 + 2 + 2, 8 + 2 + 8 + 6 + 6 + 2 + 6 + 6 + 2 + 2 + 6 + 6 + 6 + 2 + 2 + 2, 8 + 2 + 8 + 6 + 6 + 2 + 6 + 6 + 2 + 2 + 6 + 6 + 6 + 2 + 2 + 2 + 2,
            8 + 2 + 8 + 6 + 6 + 2 + 6 + 6 + 2 + 2 + 6 + 6 + 6 + 2 + 2 + 2 + 2 + 2, 8 + 2 + 8 + 6 + 6 + 2 + 6 + 6 + 2 + 2 + 6 + 6 + 6 + 2 + 2 + 2 + 2 + 2 + 2,
            8 + 2 + 8 + 6 + 6 + 2 + 6 + 6 + 2 + 2 + 6 + 6 + 6 + 2 + 2 + 2 + 2 + 2 + 2 + 4, 8 + 2 + 8 + 6 + 6 + 2 + 6 + 6 + 2 + 2 + 6 + 6 + 6 + 2 + 2 + 2 + 2 + 2 + 2 + 4 + 4,
            8 + 2 + 8 + 6 + 6 + 2 + 6 + 6 + 2 + 2 + 6 + 6 + 6 + 2 + 2 + 2 + 2 + 2 + 2 + 4 + 4 + 4, 8 + 2 + 8 + 6 + 6 + 2 + 6 + 6 + 2 + 2 + 6 + 6 + 6 + 2 + 2 + 2 + 2 + 2 + 2 + 4 + 4 + 4 + 4,
            8 + 2 + 8 + 6 + 6 + 2 + 6 + 6 + 2 + 2 + 6 + 6 + 6 + 2 + 2 + 2 + 2 + 2 + 2 + 4 + 4 + 4 + 4 + 2, 8 + 2 + 8 + 6 + 6 + 2 + 6 + 6 + 2 + 2 + 6 + 6 + 6 + 2 + 2 + 2 + 2 + 2 + 2 + 4 + 4 + 4 + 4 + 2 + 2,
            8 + 2 + 8 + 6 + 6 + 2 + 6 + 6 + 2 + 2 + 6 + 6 + 6 + 2 + 2 + 2 + 2 + 2 + 2 + 4 + 4 + 4 + 4 + 2 + 2 + 2, 8 + 2 + 8 + 6 + 6 + 2 + 6 + 6 + 2 + 2 + 6 + 6 + 6 + 2 + 2 + 2 + 2 + 2 + 2 + 4 + 4 + 4 + 4 + 2 + 2 + 2 + 5};

        byte[] timestamp = new byte[8];
        byte[] dataType = new byte[2];

        byte[] pos1 = new byte[8];
        byte[] pos2 = new byte[6];
        byte[] pos3 = new byte[6];
        byte[] pos4 = new byte[2];
        byte[] pos5 = new byte[6];
        byte[] pos6 = new byte[6];
        byte[] pos7 = new byte[2];
        byte[] pos8 = new byte[2];
        byte[] pos9 = new byte[6];
        byte[] pos10 = new byte[6];
        byte[] pos11 = new byte[6];
        byte[] pos12 = new byte[2];
        byte[] pos13 = new byte[2];
        byte[] pos14 = new byte[2];
        byte[] pos15 = new byte[2];
        byte[] pos16 = new byte[2];
        byte[] pos17 = new byte[2];
        byte[] pos18 = new byte[4];
        byte[] pos19 = new byte[4];
        byte[] pos20 = new byte[4];
        byte[] pos21 = new byte[4];
        byte[] pos22 = new byte[2];
        byte[] pos23 = new byte[2];
        byte[] pos24 = new byte[2];
        byte[] pos25 = new byte[5];
        
        
        try {
            
            pps = ppa;

        CalculationsPH ph1 = new CalculationsPH();
        CalculationsPH ph2 = new CalculationsPH();
        CalibrationManager phcc=new CalibrationManager();
        
        
        ph1.Initialize(phcc.GetS1PH4(), phcc.GetS1PH7(), phcc.GetS1PH10());
        ph2.Initialize(phcc.GetS2PH4(), phcc.GetS2PH7(), phcc.GetS2PH10());
        
        System.out.println("Calibration Status : "+phcc.CheckCal());
        System.out.println("Ph Values Sensor 1 : ph4 : "+phcc.GetS1PH4()+" ph7 : "+phcc.GetS1PH7()+" ph10: "+phcc.GetS1PH10()+" ");
        System.out.println("Ph Values Sensor 2 : ph4 : "+phcc.GetS2PH4()+" ph7 : "+phcc.GetS2PH7()+" ph10: "+phcc.GetS2PH10()+" ");
        Calculations pCal = new Calculations();
        osbtypes ptp = new osbtypes();
        StoreValues pvl = new StoreValues();

        System.arraycopy(inputData, heads[0], timestamp, 0, sizes[0]);
        System.arraycopy(inputData, heads[1], dataType, 0, sizes[1]);
        System.arraycopy(inputData, heads[2], pos1, 0, sizes[2]);
        System.arraycopy(inputData, heads[3], pos2, 0, sizes[3]);
        System.arraycopy(inputData, heads[4], pos3, 0, sizes[4]);
        System.arraycopy(inputData, heads[5], pos4, 0, sizes[5]);
        System.arraycopy(inputData, heads[6], pos5, 0, sizes[6]);
        System.arraycopy(inputData, heads[7], pos6, 0, sizes[7]);
        System.arraycopy(inputData, heads[8], pos7, 0, sizes[8]);
        System.arraycopy(inputData, heads[9], pos8, 0, sizes[9]);
        System.arraycopy(inputData, heads[10], pos9, 0, sizes[10]);
        System.arraycopy(inputData, heads[11], pos10, 0, sizes[11]);
        System.arraycopy(inputData, heads[12], pos11, 0, sizes[12]);
        System.arraycopy(inputData, heads[13], pos12, 0, sizes[13]);
        System.arraycopy(inputData, heads[14], pos13, 0, sizes[14]);
        System.arraycopy(inputData, heads[15], pos14, 0, sizes[15]);
        System.arraycopy(inputData, heads[16], pos15, 0, sizes[16]);
        System.arraycopy(inputData, heads[17], pos16, 0, sizes[17]);
        System.arraycopy(inputData, heads[18], pos17, 0, sizes[18]);
        System.arraycopy(inputData, heads[19], pos18, 0, sizes[19]);
        System.arraycopy(inputData, heads[20], pos19, 0, sizes[20]);
        System.arraycopy(inputData, heads[21], pos20, 0, sizes[21]);
        System.arraycopy(inputData, heads[22], pos21, 0, sizes[22]);
        System.arraycopy(inputData, heads[23], pos22, 0, sizes[23]);
        System.arraycopy(inputData, heads[24], pos23, 0, sizes[24]);
        System.arraycopy(inputData, heads[25], pos24, 0, sizes[25]);
        System.arraycopy(inputData, heads[26], pos25, 0, sizes[26]);

            String timestampString = new String(timestamp, "ISO-8859-1");
            System.out.println("Timestamp : "+timestampString);
            String dataTypeString = new String(dataType, "ISO-8859-1");

//            timestamps = connectionTimestamp
//                    .plusSeconds(Integer.parseInt(timestampString, 16));
            
            this.dataTypes = DataType.values()[Integer.parseInt(dataTypeString, 16)];
            int itypecheck = Integer.parseInt(dataTypeString, 16);
            if (idebug == 1) {
                System.out.println("Type : " + Integer.parseInt(dataTypeString, 16));
            }
            
            //14 iwsddata
            //9 treatment
            String temp = new String(pos1, "ISO-8859-1");
            this.timestampstr = connectionTimestamp
                    .plusSeconds(Integer.parseInt(temp, 16));

            temp = new String(pos2, "ISO-8859-1");
            MMP96302B = Integer.parseInt(temp, 16);
            if (idebug == 1) {
                System.out.println(MMP96302B);
            }
            if (itypecheck == 14) {
                if(!phcc.CheckCal())
                { 
                pvl.Make(ptp.GetTypeNumberStr("MMP96302B"), patid, MMP96302B, 0, false);
                ppa.setSzFilename("mmp630.csv");
                ppa.setTextToWrite(String.valueOf(MMP96302B));
                ppa.Appent();
                }
            }

            temp = new String(pos3, "ISO-8859-1");
            MMP98502B = Integer.parseInt(temp, 16);
            if (idebug == 1) {
                System.out.println(MMP98502B);
            }
            if (itypecheck == 14) {
                if(!phcc.CheckCal())
                {
                pvl.Make(ptp.GetTypeNumberStr("MMP98502B"), patid, MMP98502B, 0, false);
                ppa.setSzFilename("mmp850.csv");
                ppa.setTextToWrite(String.valueOf(MMP98502B));
                ppa.Appent();
                }
            }

            temp = new String(pos4, "ISO-8859-1");
            MMPSENSSTATUS1B = Integer.parseInt(temp, 16);
            if (idebug == 1) {
                System.out.println(MMPSENSSTATUS1B);
            }
            if (itypecheck == 14) {
                
                pvl.Make(ptp.GetTypeNumberStr("MMPSENSSTATUS1B"), patid, MMPSENSSTATUS1B, 0, false);
            }

            temp = new String(pos5, "ISO-8859-1");
            pH11B = Integer.parseInt(temp, 16);
            if (idebug == 1) {
                System.out.println(pH11B);
            }
            if (itypecheck == 14) {
                if(!phcc.CheckCal())
                {
                    System.out.println("ph 1 value Read : "+temp+" "+pH11B);
                Double dvar = ph1.GetPH(new Double(pH11B));
                pvl.MakeDouble(ptp.GetTypeNumberStr("pH11B"), patid, dvar);
                ppa.setSzFilename("ph1.csv");
                ppa.setTextToWrite(String.valueOf(dvar));
                ppa.Appent();
                }
                else
                {
                    if(phcc.GetPhCal().equalsIgnoreCase("14"))
                    {
                        phcc.WriteValueSensor1PH4(String.valueOf(pH11B));
                    }
                    if(phcc.GetPhCal().equalsIgnoreCase("17"))
                    {
                        phcc.WriteValueSensor1PH7(String.valueOf(pH11B));
                    }
                    if(phcc.GetPhCal().equalsIgnoreCase("110"))
                    {
                        phcc.WriteValueSensor1PH10(String.valueOf(pH11B));
                    }
                }
            }

            temp = new String(pos6, "ISO-8859-1");
            pH21B = Integer.parseInt(temp, 16);
            if (idebug == 1) {
                System.out.println(pH21B);
            }
            if (itypecheck == 14) {
                if(!phcc.CheckCal())
                {
                    System.out.println("ph 2 value Read : "+temp+" "+pH21B);
                Double dvar = ph1.GetPH(new Double(pH21B));
                pvl.MakeDouble(ptp.GetTypeNumberStr("pH21B"), patid, dvar);
                ppa.setSzFilename("ph2.csv");
                ppa.setTextToWrite(String.valueOf(dvar));
                ppa.Appent();
                }
                else
                {
                    if(phcc.GetPhCal().equalsIgnoreCase("24"))
                    {
                        phcc.WriteValueSensor2PH4(String.valueOf(pH21B));
                    }
                    if(phcc.GetPhCal().equalsIgnoreCase("27"))
                    {
                        phcc.WriteValueSensor2PH7(String.valueOf(pH21B));
                    }
                    if(phcc.GetPhCal().equalsIgnoreCase("210"))
                    {
                        phcc.WriteValueSensor2PH10(String.valueOf(pH21B));
                    }
                }
            }

            temp = new String(pos7, "ISO-8859-1");
            pH1SNESSTATUS1B = Integer.parseInt(temp, 16);
            if (idebug == 1) {
                System.out.println(pH1SNESSTATUS1B);
            }
            if (itypecheck == 14) {
                pvl.Make(ptp.GetTypeNumberStr("pH1SNESSTATUS1B"), patid, pH1SNESSTATUS1B, 0, false);
            }

            temp = new String(pos8, "ISO-8859-1");
            pH2SENS_STATUS1B = Integer.parseInt(temp, 16);
            if (idebug == 1) {
                System.out.println(pH2SENS_STATUS1B);
            }
            if (itypecheck == 14) {
                pvl.Make(ptp.GetTypeNumberStr("pH2SENS_STATUS1B"), patid, pH2SENS_STATUS1B, 0, false);
            }

            temp = new String(pos9, "ISO-8859-1");
            T11B = Integer.parseInt(temp, 16);
            if (idebug == 1) {
                System.out.println(T11B);
            }
            if (itypecheck == 14) {
                if(!phcc.CheckCal())
                {
                    System.out.println("Temp 1 Hex : "+temp);
                    System.out.println("Temp 1: "+T11B);
                Double dvar = pCal.Tempcalc(T11B, T1);
                pvl.MakeDouble(ptp.GetTypeNumberStr("T11B"), patid, dvar);
                ppa.setSzFilename("temp1.csv");
                ppa.setTextToWrite(String.valueOf(dvar));
                ppa.Appent();
                }
            }

            temp = new String(pos10, "ISO-8859-1");
            T21B = Integer.parseInt(temp, 16);
            if (idebug == 1) {
                System.out.println(T21B);
            }
            if (itypecheck == 14) {
                if(!phcc.CheckCal())
                {
                    System.out.println("Temp 2 Hex : "+temp);
                    System.out.println("Temp 2: "+T21B);
                Double dvar = pCal.Tempcalc(T21B, T2);
                pvl.MakeDouble(ptp.GetTypeNumberStr("T21B"), patid, dvar);
                ppa.setSzFilename("temp2.csv");
                ppa.setTextToWrite(String.valueOf(dvar));
                ppa.Appent();
                }
            }

            temp = new String(pos11, "ISO-8859-1");
            T31B = Integer.parseInt(temp, 16);
            if (idebug == 1) {
                System.out.println(T31B);
            }
            if (itypecheck == 14) {
                if(!phcc.CheckCal())
                {
                    System.out.println("Temp 3 Hex : "+temp);
                    System.out.println("Temp 3: "+T31B);
                Double dvar = pCal.Tempcalc(T31B, T3);
                pvl.MakeDouble(ptp.GetTypeNumberStr("T31B"), patid, dvar);
                ppa.setSzFilename("temp3.csv");
                ppa.setTextToWrite(String.valueOf(dvar));
                ppa.Appent();
                }
            }

            temp = new String(pos12, "ISO-8859-1");
            T1SENSSTATUS1B = Integer.parseInt(temp, 16);
            if (idebug == 1) {
                System.out.println(T1SENSSTATUS1B);
            }
            if (itypecheck == 14) {
                pvl.Make(ptp.GetTypeNumberStr("T1SENSSTATUS1B"), patid, T1SENSSTATUS1B, 0, false);
            }

            temp = new String(pos13, "ISO-8859-1");
            T2SENSSTATUS1B = Integer.parseInt(temp, 16);
            if (idebug == 1) {
                System.out.println(T2SENSSTATUS1B);
            }
            if (itypecheck == 14) {
                pvl.Make(ptp.GetTypeNumberStr("T2SENSSTATUS1B"), patid, T2SENSSTATUS1B, 0, false);
            }

            temp = new String(pos14, "ISO-8859-1");
            T3SENSSTATUS1B = Integer.parseInt(temp, 16);
            if (idebug == 1) {
                System.out.println(T3SENSSTATUS1B);
            }
            if (itypecheck == 14) {
                pvl.Make(ptp.GetTypeNumberStr("T3SENSSTATUS1B"), patid, T3SENSSTATUS1B, 0, false);
            }

            temp = new String(pos15, "ISO-8859-1");
            CBP1B = Integer.parseInt(temp, 16);
            if (idebug == 1) {
                System.out.println(CBP1B);
            }
            if (itypecheck == 14) {
                pvl.Make(ptp.GetTypeNumberStr("CBP1B"), patid, CBP1B, 0, false);
            }

            temp = new String(pos16, "ISO-8859-1");
            CBP_STATUS1B = Integer.parseInt(temp, 16);
            if (idebug == 1) {
                System.out.println(CBP_STATUS1B);
            }
            if (itypecheck == 14) {
                pvl.Make(ptp.GetTypeNumberStr("CBP_STATUS1B"), patid, CBP_STATUS1B, 0, false);
            }

            temp = new String(pos17, "ISO-8859-1");

            BATTSOC1B = Integer.parseInt(temp, 16);
            if (idebug == 1) {
                System.out.println(BATTSOC1B);
            }
            if (itypecheck == 14) {
                pvl.Make(ptp.GetTypeNumberStr("BATTSOC1B"), patid, BATTSOC1B, 0, false);
            }

            temp = new String(pos18, "ISO-8859-1");

            BATTCAP2B = Integer.parseInt(temp, 16);
            if (idebug == 1) {
                System.out.println(BATTCAP2B);
            }
            if (itypecheck == 14) {
                pvl.Make(ptp.GetTypeNumberStr("BATTCAP2B"), patid, BATTCAP2B, 0, false);
            }

            temp = new String(pos19, "ISO-8859-1");

            BATTV2B = Integer.parseInt(temp, 16);
            if (idebug == 1) {
                System.out.println(BATTV2B);
            }
            if (itypecheck == 14) {
                pvl.Make(ptp.GetTypeNumberStr("BATTV2B"), patid, BATTV2B, 0, false);
            }

            temp = new String(pos20, "ISO-8859-1");

            BATTI2B = Integer.parseInt(temp, 16);
            if (idebug == 1) {
                System.out.println(BATTI2B);
            }
            if (itypecheck == 14) {
                pvl.Make(ptp.GetTypeNumberStr("BATTI2B"), patid, BATTI2B, 0, false);
            }

            temp = new String(pos21, "ISO-8859-1");

            BATTTEMP2B = Integer.parseInt(temp, 16);
            if (idebug == 1) {
                System.out.println(BATTTEMP2B);
            }
            if (itypecheck == 14) {
                pvl.Make(ptp.GetTypeNumberStr("BATTTEMP2B"), patid, BATTTEMP2B, 0, false);
            }

            temp = new String(pos22, "ISO-8859-1");

            BATTSTATUS1B = Integer.parseInt(temp, 16);
            if (idebug == 1) {
                System.out.println(BATTSTATUS1B);
            }
            if (itypecheck == 14) {
                pvl.Make(ptp.GetTypeNumberStr("BATTSTATUS1B"), patid, BATTSTATUS1B, 0, false);
            }

            temp = new String(pos23, "ISO-8859-1");

            IWSDATTITUDE1B = Integer.parseInt(temp, 16);
            if (idebug == 1) {
                System.out.println(IWSDATTITUDE1B);
            }
            if (itypecheck == 14) {
                pvl.Make(ptp.GetTypeNumberStr("IWSDATTITUDE1B"), patid, IWSDATTITUDE1B, 0, false);
            }

            temp = new String(pos24, "ISO-8859-1");
            IWSDSTATUS1B = Integer.parseInt(temp, 16);
            if (idebug == 1) {
                System.out.println(IWSDSTATUS1B);
            }
            if (itypecheck == 14) {
                pvl.Make(ptp.GetTypeNumberStr("IWSDSTATUS1B"), patid, IWSDSTATUS1B, 0, false);
            }

            temp = new String(pos25, "ISO-8859-1");
            data = Integer.parseInt(temp, 16);
            if (idebug == 1) {
                System.out.println("TREATMENT : " + data);
            }
            if (itypecheck == 9) {

                pvl.MakeTreatment(ptp.GetTypeNumberStr("TREATMENT"), patid, data, 0, false);
                System.out.println("Treatment Value : "+data);
            }

            if (itypecheck == 17) {
                pvl.MakeAlarm(patid, "Low Pressure");
                System.out.println("Low Pressure Alarm");
            }
            if (itypecheck == 16) {
                pvl.MakeAlarm(patid, "High Pressure");
                System.out.println("High Pressure Alarm");
            }
            if (itypecheck == 15) {
                pvl.MakeAlarm(patid, "Over Pressure");
                System.out.println("Over Pressure Alarm");
            }
            

        } catch (Exception e) {
            System.out.println("Encoding issue : " + e.getMessage());
        }

    }

}
