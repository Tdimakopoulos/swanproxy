/*
 * Copyright (c) 2014 EXODUS S.A.
 * SwaniCare
 * Author : Dr. Thomas Dimakopoulos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package org.eu.exus.swan.network;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum SnpdErrorCode {

    ACTIVITY_COMMUNICATION_ERROR (-2),
    SENSOR_MMP_ERROR (-3),
    SENSOR_TNFA_ERROR (-4),
    SENSOR_CRP_ERROR (-5),
    SENSOR_TEMP_ERROR (-6),
    SENSOR_PH_ERROR (-7),
    SENSOR_BACTERIA_ERROR (-8),
    SENSOR_FUSION_FAILURE (-9),
    PUMP_COMMUNICATION_ERROR (-10),
    PUMP_MALFUNCTION_ERROR (-11),
    PUMP_CALCULATION_ERROR (-12),
    WATCHDOG_SET_ERROR (-13);

    private static final Map<Integer, SnpdErrorCode> lookup = new HashMap<>();

    static {
        for(SnpdErrorCode s : EnumSet.allOf(SnpdErrorCode.class))
            lookup.put(s.getErrorCode(), s);
    }

    private final int errorCode;

    SnpdErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public int getErrorCode() { return errorCode; }

    public static SnpdErrorCode get(int errorCode) {
        return lookup.get(errorCode);
    }

}
