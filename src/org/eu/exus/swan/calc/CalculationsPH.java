/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eu.exus.swan.calc;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class CalculationsPH {

    Double m1, n1, m2, n2;
    Double x1, x2, x3, y1, y2, y3;

    public void Initialize(Double ph4, Double ph7, Double ph10) {
        y1 = new Double(4);
        y2 = new Double(7);
        y3 = new Double(10);
        x1 = ph4;
        x2 = ph7;
        x3 = ph10;
        CalculateCurve();
    }

    private void CalculateCurve() {
        m1 = (y2 - y1) / (x2 - x1);
        n1 = y1 - (m1 * x1);
        m2 = (y3 - y2) / (x3 - x2);
        n2 = y2 - (m2 * x2);
    }

    public Double GetPH(Double x4) {
//        System.out.println("Calculate ph start");
        Double y4;
        if (x4 < x2) {
            y4 = m1 * x4 + n1;
        } else if (x4 > x2) {
            y4 = m2 * x4 + n2;
        } else {
            y4 = y2;
        }
//        System.out.println("Calculate ph end");
System.out.println("Calculate ph curve :"+y4);
        return y4;
    }
}
