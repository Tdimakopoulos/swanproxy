/*
 * Copyright (c) 2014 EXODUS S.A.
 * SwaniCare
 * Author : Dr. Thomas Dimakopoulos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.eu.exus.swan.calc;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class Calculations {

    //R = R0*(ln^-1(((1/Tk)-(1/Tk0))*B))
    //Tc = (Tk - 273.15)
    //1/Tk = 1/Tk0 + (1/B)*ln(R/R0)
    //R0	10000
    //Tk0	298.15
    //B(1)	3610
    //B(2)	3694
    //TKTC	273.15
    public Double Tempcalc(int ivalue, int B) {

        double b55 = ivalue;

        double o22 = 2057675;
        double o23 = -3748.42;
        double g22 = 68000;
        double g23 = 68000;
        double g24 = 10000;
        double g25 = 3;
        double k22 = 0.2777;
        double k24 = 6.3875;
        double k23 = 1.024;

        double c22 = 10000;
        double c23 = 298.15;
        double c24 = 3610;
        double c25 = 3694;
        double c26 = 273.15;

        double f55 = (b55 - o23) / o22;

        double j55 = (f55 - k23) / (5 + (5 * k22));
        double l55 = ((g25 * g22 * g24) + j55 * g22 * (g23 + g24)) / ((g25 * g23) - j55 * (g23 + g24));
        double t = -c26 + 1 / ((1 / c23) + ((1 / c25) * Math.log(l55 / c22)));
        System.out.println("Input temp value int : "+ivalue);
        System.out.println("Input temp value double : "+b55);
        System.out.println("Calculate temp new method : "+t);
        return t;
    }

    public Double Tempcalcx(int ivalue, int B) {

        Double bcalc = Double.valueOf(ivalue) / 10;
        Double bcal1 = 1 / 298.15;
        Double calc = bcal1 + (1 / B) * Math.log(bcalc);
        System.out.println("Calculated temperature : " + ((1 / calc) - 273.15));

        return ((1 / calc) - 273.15);
    }

//    public Double Tempcalcxdd(int ivalue,int B) {
//
//        System.out.println("temp vOut : "+ivalue);
//        Double T0=298.15;
//        
//        Double R=new Double(10);
//        
//        Double R0=new Double(2252);
//        Double Bx=new Double(3892);
//        
//        
//        Double drs=((ivalue*2.048*2)-1.024)*1300;
//        
//        R=(drs)+R0;
//        
//        Double d=(1/T0)+(1/Bx)*Math.log(R/R0);
//        System.out.println("drs "+drs);
//        System.out.println("R "+R);
//        System.out.println("D "+d);
//        
////        ///// Sostos tropos
////        System.out.println("Calculate temp start");
////        Long bcalc = Long.valueOf(ivalue) / 10;
////        Double bcal1 = 1 / 298.15;
////        Double calc = bcal1 + (1 / B) * Math.log(bcalc);
////        System.out.println("Calculate temp end");
////        /// Sostos tropos
////        return 1 / calc;
//        System.out.println("Calculate temp :"+((1/d)-273.15));
//        return (1/d)-273.15;
//    }
    public Long phcalc(int ivalue) {
        int itemp = 0;
        return Long.valueOf(itemp);
    }

    public Long MMPcalc(int ivalue) {
        int itemp = 0;
        return Long.valueOf(itemp);
    }

    public Long Pressurecalc(int ivalue) {
        int itemp = 0;
        return Long.valueOf(itemp);
    }

}
