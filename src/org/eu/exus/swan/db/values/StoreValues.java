/*
 * Copyright (c) 2014 EXODUS S.A.
 * SwaniCare
 * Author : Dr. Thomas Dimakopoulos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.eu.exus.swan.db.values;

import java.util.Date;
import org.hl7.ws.obs.Hl7Observation;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class StoreValues {

    public void Make(String msgv, Long patientid, int value, long lvalue, boolean blong) {
        org.hl7.ws.obs.Hl7Observation hL7Observatio = new Hl7Observation();
        hL7Observatio.setLpatient(patientid);
        hL7Observatio.setName(msgv);
        hL7Observatio.setLngdate((new Date()).getTime());
        if (!blong) {
            hL7Observatio.setIntvalue(value);
        }
        if (blong) {
            hL7Observatio.setLongvalue(lvalue);
        }
        hL7Observatio.setStrvalue(msgv);
        create(hL7Observatio);
    }
    
    public void MakeTreatment(String msgv, Long patientid, int value, long lvalue, boolean blong) {
//        org.hl7.ws.obs.Hl7Observation hL7Observatio = new Hl7Observation();
//        hL7Observatio.setLpatient(patientid);
//        hL7Observatio.setName(msgv);
//        hL7Observatio.setLngdate((new Date()).getTime());
//        if (!blong) {
//            hL7Observatio.setIntvalue(value);
//        }
//        if (blong) {
//            hL7Observatio.setLongvalue(lvalue);
//        }
//        hL7Observatio.setStrvalue(msgv);
//        create(hL7Observatio);
org.hl7.ws.realtime.Hl7ObservationRealTime hL7ObservationRealTime=new org.hl7.ws.realtime.Hl7ObservationRealTime();
hL7ObservationRealTime.setLpatient(patientid);
hL7ObservationRealTime.setLngdate((new Date()).getTime());
if (blong) {
hL7ObservationRealTime.setLongvalue(lvalue);
}
if (!blong) {
hL7ObservationRealTime.setIntvalue(value);
}
create_2(hL7ObservationRealTime);
    }

    public void MakeDouble(String msgv, Long patientid, Double dValue) {
        org.hl7.ws.obs.Hl7Observation hL7Observatio = new Hl7Observation();
        hL7Observatio.setLpatient(patientid);
        hL7Observatio.setName(msgv);
        hL7Observatio.setLngdate((new Date()).getTime());

        hL7Observatio.setStrvalue(String.valueOf(dValue));
        create(hL7Observatio);
    }

    private static void create(org.hl7.ws.obs.Hl7Observation hL7Observation) {
        org.hl7.ws.obs.Obshl7_Service service = new org.hl7.ws.obs.Obshl7_Service();
        org.hl7.ws.obs.Obshl7 port = service.getObshl7Port();
        port.create(hL7Observation);
    }

    public void MakeAlarm(Long patientid, String alarm) {
        org.swan.alarm.ws.Swanalarmdb swanalarmdb = new org.swan.alarm.ws.Swanalarmdb();
        swanalarmdb.setPatientid(patientid);
        swanalarmdb.setAlarm1(alarm);
        swanalarmdb.setAlarm2(alarm);
        swanalarmdb.setAlarm3(alarm);
        swanalarmdb.setAlarm4(alarm);
        create_1(swanalarmdb);
    }

    private static void create_1(org.swan.alarm.ws.Swanalarmdb swanalarmdb) {
        org.swan.alarm.ws.SwanAlarm_Service service = new org.swan.alarm.ws.SwanAlarm_Service();
        org.swan.alarm.ws.SwanAlarm port = service.getSwanAlarmPort();
        port.create(swanalarmdb);
    }

    private static void create_2(org.hl7.ws.realtime.Hl7ObservationRealTime hL7ObservationRealTime) {
        org.hl7.ws.realtime.RealTimeOBS_Service service = new org.hl7.ws.realtime.RealTimeOBS_Service();
        org.hl7.ws.realtime.RealTimeOBS port = service.getRealTimeOBSPort();
        port.create(hL7ObservationRealTime);
    }

}
