/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.exus.swan.phcal;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import org.exus.rnd.file.FileUtils;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class CalibrationManager {

    FileUtils pFU = new FileUtils();

    public boolean CheckCal() {
        return pFU.CheckFileExists(pFU.GetPathMD1() + "start.ph");
    }

    public String GetPhCal() {
        return pFU.GetPHFileDetails(pFU.GetPathMD1() + "c.ph");
    }

    public void WriteValueSensor1PH4(String ph) {
        pFU.writeonfile(pFU.GetPathMD1() + "S1P4.dat", ph);
    }

    public void WriteValueSensor1PH7(String ph) {
        pFU.writeonfile(pFU.GetPathMD1() + "S1P7.dat", ph);
    }

    public void WriteValueSensor1PH10(String ph) {
        pFU.writeonfile(pFU.GetPathMD1() + "S1P10.dat", ph);
    }

    public void WriteValueSensor2PH4(String ph) {
        pFU.writeonfile(pFU.GetPathMD1() + "S2P4.dat", ph);
    }

    public void WriteValueSensor2PH7(String ph) {
        pFU.writeonfile(pFU.GetPathMD1() + "S2P7.dat", ph);
    }

    public void WriteValueSensor2PH10(String ph) {
        pFU.writeonfile(pFU.GetPathMD1() + "S2P10.dat", ph);
    }
    
    public Double GetS1PH4()
    {
         try (BufferedReader br = new BufferedReader(new FileReader(pFU.GetPathMD1() + "S1P4.dat"))) {
            String line;
            while ((line = br.readLine()) != null) {
                return Double.valueOf(line);
            }
        }catch(IOException e)
        {
            return new Double(0);
        }
        return new Double(0);
    }
    public Double GetS1PH7()
    {
         try (BufferedReader br = new BufferedReader(new FileReader(pFU.GetPathMD1() + "S1P7.dat"))) {
            String line;
            while ((line = br.readLine()) != null) {
                return Double.valueOf(line);
            }
        }catch(IOException e)
        {
            return new Double(0);
        }
        return new Double(0);
    }
    public Double GetS1PH10()
    {
         try (BufferedReader br = new BufferedReader(new FileReader(pFU.GetPathMD1() + "S1P10.dat"))) {
            String line;
            while ((line = br.readLine()) != null) {
                return Double.valueOf(line);
            }
        }catch(IOException e)
        {
            return new Double(0);
        }
        return new Double(0);
    }
    /////////////////////////////////////////////////
    
    public Double GetS2PH4()
    {
         try (BufferedReader br = new BufferedReader(new FileReader(pFU.GetPathMD1() + "S2P4.dat"))) {
            String line;
            while ((line = br.readLine()) != null) {
                return Double.valueOf(line);
            }
        }catch(IOException e)
        {
            return new Double(0);
        }
        return new Double(0);
    }
    public Double GetS2PH7()
    {
         try (BufferedReader br = new BufferedReader(new FileReader(pFU.GetPathMD1() + "S2P7.dat"))) {
            String line;
            while ((line = br.readLine()) != null) {
                return Double.valueOf(line);
            }
        }catch(IOException e)
        {
            return new Double(0);
        }
        return new Double(0);
    }
    public Double GetS2PH10()
    {
         try (BufferedReader br = new BufferedReader(new FileReader(pFU.GetPathMD1() + "S2P10.dat"))) {
            String line;
            while ((line = br.readLine()) != null) {
                return Double.valueOf(line);
            }
        }catch(IOException e)
        {
            return new Double(0);
        }
        return new Double(0);
    }
}
