/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.exus.swan.timer;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.exus.rnd.statistics.basic.Average;
import org.exus.rnd.statistics.basic.Max;
import org.exus.rnd.statistics.basic.Min;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class timer {

    Timer timer = new Timer();
    int ipos = 0;
    int istart = 10;
    String path;
    String file;
    int itype;//1=ph1 2=ph2 3=tmp1 4=tmp2 5=tmp3
    Long patid;

    public void Initialize(int isecs) {
        istart = isecs;
    }

    public void SetPathFile(String path, String file) {
        this.path = path;
        this.file = file;
    }

    public void SetDetails(Long ppid, int type) {
        patid = ppid;
        itype = type;
    }

    public void starttimer() {
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

//                System.out.println("Timer Tik type : " + itype);
                ipos = ipos + 1;
                if (itype == 6) {
//                        System.out.println("===  "+ipos+"  -----  "+istart);
                    }
                if (ipos == istart) {
                    if (itype == 6) {
                        CalcMMP();
                    } else {
                        Calcs();
                    }
                    ipos = 0;
                }
            }
        }, 1000, 1000);
    }

    private void DeleteFile(String fpath) {
        try {

            File file = new File(fpath);

            if (file.delete()) {
                System.out.println(file.getName() + " is Cleared!");
            } else {
                System.out.println("Delete operation is failed.");
            }

        } catch (Exception e) {

            System.out.println("Delete operation is failed." + e.getMessage());

        }
    }

    private void CalcMMP() {
        try {
            System.out.println("SSS  1");
            List<Double> doubleList = new ArrayList<Double>();
            Scanner scanner = new Scanner(new File(path + "mmp630.csv"));
            while (scanner.hasNextDouble()) {
                double value = scanner.nextDouble();
                doubleList.add(value);
            }
            scanner.close();
System.out.println("SSS  2");
            List<Double> doubleList2 = new ArrayList<Double>();
            Scanner scanner2 = new Scanner(new File(path + "mmp850.csv"));
            while (scanner2.hasNextDouble()) {
                double value = scanner2.nextDouble();
                doubleList2.add(value);
            }
            scanner2.close();
            System.out.println("SSS  3");
        List<Double> ratio = new ArrayList<Double>();
        List<Double> delta = new ArrayList<Double>();
        List<Double> slope = new ArrayList<Double>();
        int imaxx=0;
        if (doubleList.size()==doubleList2.size())
            imaxx=doubleList.size();
        else if (doubleList.size()>doubleList2.size())
            imaxx=doubleList2.size();
        else if (doubleList.size()<doubleList2.size())
            imaxx=doubleList.size();
        for (int i=0;i<imaxx;i++)
        {
            ratio.add(Math.log10(doubleList2.get(i)/doubleList.get(i)));
            
        }
        System.out.println("SSS  4");
        for (int i=0;i<ratio.size()-1;i++)
        {
            delta.add(ratio.get(i)-ratio.get(i+1));
        }
        System.out.println("SSS  5");
        int icalcs=0;
        Double avg = new Double(0);
        for (int i=0;i<delta.size();i++)
        {
            icalcs=icalcs+1;
            avg=avg+delta.get(i);
            
            if(icalcs==30)
            {   slope.add(avg/icalcs);
                icalcs=0;
                avg = new Double(0);
            }
        }
        System.out.println("SSS  6");
        Double SLOPE=new Double(0);
        Max pMax=new Max();
        SLOPE=pMax.getMaxDouble(slope);
        
        //tdim : add interface for B and A
        Double mmp =(SLOPE-0)/0.00006;
        System.out.println("SSS  mmp="+mmp);
        //ratio = mmp850/mmp630 then log 10 -> save to list
        //differnce between two
        //slope i = every 30 take max
        //max of slope array = slope
        // mmp = =(SLOPE-0)/0.00006
        
        org.hl7.ws.obs.summary.Hl7ObservationsSummary hL7ObservationsSummary = new org.hl7.ws.obs.summary.Hl7ObservationsSummary();
            hL7ObservationsSummary.setLpatient(patid);
            hL7ObservationsSummary.setName(String.valueOf(this.itype));
            hL7ObservationsSummary.setLngdate((new Date()).getTime());
            hL7ObservationsSummary.setInfo1(String.valueOf(mmp));
            hL7ObservationsSummary.setInfo2("");
            hL7ObservationsSummary.setInfo3("");
            create(hL7ObservationsSummary);

            DeleteFile(path + "mmp630.csv");
            DeleteFile(path + "mmp850.csv");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(timer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
    }

    private void Calcs() {
        try {
            List<Double> doubleList = new ArrayList<Double>();
            Scanner scanner = new Scanner(new File(path + file));
            while (scanner.hasNextDouble()) {
                double value = scanner.nextDouble();
                doubleList.add(value);
            }
            scanner.close();
            Average pAvg = new Average();
            Double pd = pAvg.calculateAverageDouble(doubleList);
            System.out.println("AVG : " + pd);
            Min pMin = new Min();
            int m = pMin.getIndexOfMinDouble(doubleList);
            System.out.println("Min : " + m);
            Max pMax = new Max();
            Double mx = pMax.getMaxDouble(doubleList);
            System.out.println("Max : " + mx);

            org.hl7.ws.obs.summary.Hl7ObservationsSummary hL7ObservationsSummary = new org.hl7.ws.obs.summary.Hl7ObservationsSummary();
            hL7ObservationsSummary.setLpatient(patid);
            hL7ObservationsSummary.setName(String.valueOf(this.itype));
            hL7ObservationsSummary.setLngdate((new Date()).getTime());
            hL7ObservationsSummary.setInfo1(String.valueOf(pd));
            hL7ObservationsSummary.setInfo2(String.valueOf(doubleList.get(m)));
            hL7ObservationsSummary.setInfo3(String.valueOf(mx));
            create(hL7ObservationsSummary);

            DeleteFile(path + file);
        } catch (FileNotFoundException ex) {
            System.out.println("File not exists! " + file);
        }
    }

    private static void create(org.hl7.ws.obs.summary.Hl7ObservationsSummary hL7ObservationsSummary) {
        org.hl7.ws.obs.summary.ObservationsSummary_Service service = new org.hl7.ws.obs.summary.ObservationsSummary_Service();
        org.hl7.ws.obs.summary.ObservationsSummary port = service.getObservationsSummaryPort();
        port.create(hL7ObservationsSummary);
    }
}
