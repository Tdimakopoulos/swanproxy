/*
 * Copyright (c) 2014 EXODUS S.A.
 * SwaniCare
 * Author : Dr. Thomas Dimakopoulos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.exus.swan.proxy;

import org.eu.exus.swan.network.SnpdPacket;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.time.Instant;
import java.util.Timer;
import java.util.TimerTask;
import org.exus.rnd.file.FileUtils;
import org.exus.rnd.system.properties.SystemProperties;
import org.exus.swan.timer.timer;

public class Main {

    static final int DEFAULT_PORT = 1144;

    public static void main(String[] args) throws SocketException, FileNotFoundException, IOException {
//        int i = 8 + 2 + 8 + 6 + 6 + 2 + 6 + 6 + 2 + 2 + 6 + 6 + 6 + 2 + 2 + 2 + 2 + 2 + 2 + 4 + 4 + 4 + 4 + 2 + 2 + 2 + 5;
//
//        System.out.println("Package Size to be valid : " + i);
        SystemProperties ppa = new SystemProperties();
//        int[] sizes = new int[]{8, 2, 8, 6, 6, 2, 6, 6, 2, 2, 6, 6, 6, 2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 2, 2, 2, 4};
        String szfilename = System.getProperty("user.dir");
        System.out.println("Reading settings file : " + szfilename + ppa.FileSeperator() + "setting.cnv");;
        FileUtils pp = new FileUtils();

        pp.setSzPath(szfilename + ppa.FileSeperator());

        FileInputStream fstream = new FileInputStream(szfilename + ppa.FileSeperator() + "setting.cnv");
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

        String strLine;
        strLine = br.readLine();
        System.out.println("Port : " + strLine);
        int portNumber = Integer.parseInt(strLine);
        strLine = br.readLine();
        System.out.println("Patient ID : " + strLine);
        Long patientid = Long.parseLong(strLine);
        strLine = br.readLine();
        System.out.println("T1 : " + strLine);
        int T1 = Integer.valueOf(strLine);
        strLine = br.readLine();
        System.out.println("T2 : " + strLine);
        int T2 = Integer.valueOf(strLine);
        strLine = br.readLine();
        System.out.println("T3 : " + strLine);
        int T3 = Integer.valueOf(strLine);
        br.close();
        System.out.println("Listening on port " + portNumber + "...");

        int itimersec = 10 * 60;
        //ph1
        timer ptimer = new timer();
        ptimer.Initialize(itimersec);
        ptimer.SetPathFile(szfilename + ppa.FileSeperator(), "ph1.csv");
        ptimer.SetDetails(patientid, 1);

        timer ptimerx = new timer();
        ptimerx.Initialize(itimersec);
        ptimerx.SetPathFile(szfilename + ppa.FileSeperator(), "ph2.csv");
        ptimerx.SetDetails(patientid, 2);

        timer ptimerx1 = new timer();
        ptimerx1.Initialize(itimersec);
        ptimerx1.SetPathFile(szfilename + ppa.FileSeperator(), "temp1.csv");
        ptimerx1.SetDetails(patientid, 3);

        timer ptimerx2 = new timer();
        ptimerx2.Initialize(itimersec);
        ptimerx2.SetPathFile(szfilename + ppa.FileSeperator(), "temp2.csv");
        ptimerx2.SetDetails(patientid, 4);

        timer ptimerx21 = new timer();
        ptimerx21.Initialize(itimersec);
        ptimerx21.SetPathFile(szfilename + ppa.FileSeperator(), "temp3.csv");
        ptimerx21.SetDetails(patientid, 5);

        timer ptimerslope = new timer();
        ptimerslope.Initialize(120 * 60);
        ptimerslope.SetPathFile(szfilename + ppa.FileSeperator(), "step1.csv");
        ptimerslope.SetDetails(patientid, 6);

        Instant connectionTimestamp = Instant.now();
        int imaxsizeread = 105;
        byte[] data = new byte[imaxsizeread];
        boolean firstConnection = true;

        while (true) {
            try (
                    ServerSocket serverSocket = new ServerSocket(portNumber);
                    Socket clientSocket = serverSocket.accept();
                    DataInputStream dataInputStream = new DataInputStream(clientSocket.getInputStream())) {
                serverSocket.setSoTimeout(10000);

                System.out.println(clientSocket.getRemoteSocketAddress() + " just connected.");

                if (firstConnection == true) {
                    connectionTimestamp = Instant.now();
                    firstConnection = false;
                }

                boolean brun = true;
                ptimer.starttimer();
                ptimerx.starttimer();
                ptimerx1.starttimer();
                ptimerx21.starttimer();
                ptimerx2.starttimer();
                ptimerslope.starttimer();
                while (brun) {
                    int packetSize = dataInputStream.read(data, 0, data.length);
                    if (packetSize == -1) {
                        brun = false;
                        System.out.println("Connection from " + clientSocket.getRemoteSocketAddress() + " closed !!");
                    }
                    if (packetSize == imaxsizeread) {
                        String dataString = new String(data, "ISO-8859-1");

                        FileWriter fw = new FileWriter(szfilename + ppa.FileSeperator() + "data.raw", true);
                        fw.append(dataString);
                        fw.append(System.getProperty("line.separator"));
                        fw.close();
                        SnpdPacket newPacket = new SnpdPacket(data, connectionTimestamp, patientid, T1, T2, T3, pp);
                    }
                }

            } catch (IOException e) {
                System.out.println("Exception caught when trying to listen on port "
                        + portNumber + " or listening for a connection");
                System.out.println(e.getMessage());
            }

            System.out.println("Connection closed. Trying again...");
        }
    }

}
